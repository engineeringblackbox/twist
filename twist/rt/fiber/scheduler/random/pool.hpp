#pragma once

#include <twist/rt/fiber/system/fwd.hpp>
#include <twist/rt/fiber/system/limits.hpp>

#include <cstdint>

namespace twist::rt::fiber {

namespace system::scheduler::random {

// Deque-based fiber queue with fast O(1) PickRandom

class FiberPool {
  using Fiber = fiber::system::Fiber;

 public:
  void Push(system::Fiber* f) {
    WHEELS_VERIFY(!IsFull(), "Twist thread limit reached: " << kMaxFibers);
    buf_[size_++] = f;
  }

  bool IsEmpty() const {
    return size_ == 0;
  }

  bool IsFull() const {
    return size_ == kMaxFibers;
  }

  Fiber* PopAll() {
    if (size_ == 0) {
      return nullptr;
    }
    Fiber* pop = buf_[size_ - 1];
    --size_;
    return pop;
  }

  // For fault injection, O(1)
  Fiber* PopRandom(uint64_t r) {
    if (size_ == 0) {
      return nullptr;
    }

    size_t index = r % size_;

    Fiber* f = buf_[index];
    buf_[index] = buf_[size_ - 1];
    --size_;

    return f;
  }

  // O(size),
  // For timeouts, so no reason to optimize
  bool Remove(Fiber* f) {
    for (size_t i = 0; i < size_; ++i) {
      if (buf_[i] == f) {
        buf_[i] = buf_[size_ - 1];
        --size_;
        return true;
      }
    }

    return false;  // Not found
  }

 private:
  std::array<Fiber*, kMaxFibers> buf_;
  size_t size_ = 0;
};

}  // namespace system::scheduler::random

}  // twist::rt::fiber
