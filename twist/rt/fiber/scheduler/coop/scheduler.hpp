#pragma once

#include <twist/rt/fiber/system/scheduler.hpp>

#include "params.hpp"

#include <twist/rt/fiber/system/simulator.hpp>

#include <twist/wheels/random/wyrand.hpp>

#include <wheels/core/assert.hpp>
#include <wheels/intrusive/list.hpp>

namespace twist::rt::fiber {

namespace system::scheduler::coop {

class Scheduler final : public IScheduler {
 public:
  using Params = coop::Params;

 private:
  // Futex queue

  class WaitQueue : public IWaitQueue {
   public:
    bool IsEmpty() const override {
      return waiters_.IsEmpty();
    }

    void Push(Fiber* waiter) override {
      return waiters_.PushBack(waiter);
    }

    Fiber* Pop() override {
      return waiters_.PopFront();
    }

    Fiber* PopAll() override {
      return Pop();
    }

    bool Remove(Fiber* fiber) override {
      if (waiters_.IsLinked(fiber)) {
        waiters_.Unlink(fiber);
        return true;
      } else {
        return false;
      }
    }

   private:
    wheels::IntrusiveList<Fiber, SchedulerTag> waiters_;
  };

 public:
  Scheduler(Params params = Params())
      : params_(params),
        random_(params_.seed) {
    WHEELS_UNUSED(params);
  }

  bool NextSchedule() override {
    return false;
  }

  ~Scheduler() {
    WHEELS_VERIFY(run_queue_.IsEmpty(), "Non-empty run queue");
  }

  void Start(Simulator*) override {
    // No-op
  }

  // System

  void Interrupt(Fiber* active) override {
    run_queue_.PushFront(active);
  }

  void Yield(Fiber* active) override {
    run_queue_.PushBack(active);
  }

  void Wake(Fiber* waiter) override {
    run_queue_.PushBack(waiter);
  }

  void Spawn(Fiber* fiber) override {
    run_queue_.PushBack(fiber);
  }

  void Exit(Fiber*) override {
    // No-op
  }

  // Run queue

  bool IsIdle() const override {
    return run_queue_.IsEmpty();
  }

  Fiber* PickNext() override {
    return run_queue_.PopFront();
  }

  void Remove(Fiber* fiber) override {
    if (run_queue_.IsLinked(fiber)) {
      run_queue_.Unlink(fiber);
    }
  }

  // Hints

  void LockFree(Fiber* /*fiber*/, bool /*flag*/) override {
    // No-op
  }

  void Progress(Fiber* /*fiber*/) override {
    // No-op
  }

  void NewIter() override {
    // No-op
  }

  // Futex

  IWaitQueuePtr NewWaitQueue() override {
    return std::make_unique<WaitQueue>();
  }

  // Random

  uint64_t RandomNumber() override {
    return random_.Next();
  }

  size_t RandomChoice(size_t alts) override {
    return random_.Next() % alts;
  }

  // Spurious

  bool SpuriousWakeup() override {
    return false;
  }

  bool SpuriousTryFailure() override {
    return false;
  }

 private:
  const Params params_;
  twist::random::WyRand random_;

  wheels::IntrusiveList<Fiber, SchedulerTag> run_queue_;
};

}  // namespace system::scheduler::coop

}  // namespace twist::rt::fiber
