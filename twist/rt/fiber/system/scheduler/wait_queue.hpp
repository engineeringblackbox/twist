#pragma once

#include "../fwd.hpp"

#include <memory>

namespace twist::rt::fiber {

namespace system::scheduler {

struct IWaitQueue {
  virtual ~IWaitQueue() = default;

  virtual bool IsEmpty() const = 0;
  virtual void Push(Fiber*) = 0;
  virtual Fiber* Pop() = 0;
  virtual Fiber* PopAll() = 0;
  virtual bool Remove(Fiber*) = 0;
};

using IWaitQueuePtr = std::unique_ptr<IWaitQueue>;

}  // namespace system::scheduler

}  // namespace twist::rt::fiber
