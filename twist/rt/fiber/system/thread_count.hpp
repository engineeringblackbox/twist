#pragma once

#include <cstdlib>

namespace twist::rt::fiber {

namespace system {

size_t ThreadCount();

}  // namespace system

}  // namespace twist::rt::fiber
