#pragma once

#include <twist/rt/fiber/user/syscall/write.hpp>

namespace twist::rt::fiber {

namespace user::library::system {

inline void Write(int fd, std::string_view buf) {
  syscall::Write(fd, buf);
}

}  // namespace user::library::system

}  // namespace twist::rt::fiber
