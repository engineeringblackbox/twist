#pragma once

#include <twist/rt/fiber/user/syscall/id.hpp>

// #include <wheels/core/assert.hpp>
#include <twist/rt/fiber/user/safety/assert.hpp>

namespace twist::rt::fiber {

namespace user {

class MutexOwner {
 public:
  void Lock() {
    id_ = syscall::GetId();
  }

  bool Own() const {
    return id_ == syscall::GetId();
  }

  system::FiberId Id() const {
    return id_;
  }

  void Unlock() {
    ___TWIST_FIBER_USER_VERIFY(
        Own(),
        "The mutex must be locked by the current thread of execution");
    Reset();
  }

 private:
  void Reset() {
    id_ = system::kImpossibleFiberId;
  }

 private:
  system::FiberId id_ = system::kImpossibleFiberId;
};

}  // namespace user

}  // namespace twist::rt::fiber
