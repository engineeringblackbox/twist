#include "wait_group.hpp"

#include <twist/rt/fiber/user/syscall/futex.hpp>
#include <twist/rt/fiber/user/scheduler/preemption_guard.hpp>

namespace twist::rt::fiber {

namespace user::test {

void WaitGroup::Join(wheels::SourceLocation call_site) {
  if (count_ == 0) {
    return;
  }

  {
    // NB: Happens-before any wg thread
    left_.store(count_);
  }

  // Spawn fibers

  UserStateNode* s = head_;
  while (s != nullptr) {
    syscall::Spawn(s);
    s = s->next;
  }

  do {
    system::WaiterContext waiter{system::FutexType::WaitGroup,
                                 "WaitGroup::Join", call_site};
    syscall::FutexWait(&done_, 0, &waiter);
  } while (done_.load() != 1);
}

void WaitGroup::AtFiberExit() noexcept {
  scheduler::PreemptionGuard guard;  // <- Merge with the last user action

  // NB: Join release sequences of wg threads
  if (left_.fetch_sub(1) == 1) {
    done_.store(1);
    syscall::FutexWake(&done_, 1);
  }
}

}  // namespace user::test

}  // namespace twist::rt::fiber
