#pragma once

#include <twist/rt/fiber/user/scheduler/interrupt.hpp>

#include <wheels/core/source_location.hpp>

#include <cstdlib>
// std::hash
#include <utility>

namespace twist::rt::fiber {

namespace user::assist {

// New

template <typename T, typename ... Args>
T* NewImpl(wheels::SourceLocation source_loc, Args&& ... args) {
  {
    // DPOR: artificial dependency for allocations of the same type T
    // (e.g. Node in lock-free stack)
    // void* var = (void*)(typeid(T).hash_code() & 1);

    // TODO: use var addr above
    user::scheduler::Interrupt(source_loc);
  }

  return new T{std::forward<Args>(args)...};
}

template <typename T>
T* New(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
  return NewImpl<T>(call_site);
}

template <typename T, typename A>
T* New(A&& a, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
  return NewImpl<T>(call_site, std::forward<A>(a));
}

template <typename T, typename A, typename B>
T* New(A&& a, B&& b, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
  return NewImpl<T>(call_site, std::forward<A>(a), std::forward<B>(b));
}

template <typename T, typename A, typename B, typename C>
T* New(A&& a, B&& b, C&& c, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
  return NewImpl<T>(call_site, std::forward<A>(a), std::forward<B>(b), std::forward<C>(c));
}

template <typename T, typename A, typename B, typename C, typename D>
T* New(A&& a, B&& b, C&& c, D&& d, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
  return NewImpl<T>(call_site, std::forward<A>(a), std::forward<B>(b), std::forward<C>(c), std::forward<D>(d));
}

template <typename T, typename A, typename B, typename C, typename D, typename E>
T* New(A&& a, B&& b, C&& c, D&& d, E&& e, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
  return NewImpl<T>(call_site, std::forward<A>(a), std::forward<A>(a), std::forward<B>(b), std::forward<C>(c), std::forward<D>(d), std::forward<E>(e));
}

// Memory access

void MemoryAccess(void* addr, size_t size, wheels::SourceLocation call_site = wheels::SourceLocation::Current());

// Ptr<T>

template <typename T>
struct Ptr {
  Ptr(T* p = nullptr, wheels::SourceLocation loc = wheels::SourceLocation::Current())
      : raw(p), loc_(loc) {
  }

  // operator T*

  operator T*() {
    return raw;
  }

  operator const T*() const {
    return raw;
  }

  // operator ->

  T* operator->() {
    Access(loc_);
    return raw;
  }

  const T* operator->() const {
    Access(loc_);
    return raw;
  }

  // operator *

  T& operator*() {
    Access(loc_);
    return *raw;
  }

  const T& operator*() const {
    Access(loc_);
    return *raw;
  }

  // operator &

  T** operator&() {
    return &raw;
  }

  T* const * operator&() const {
    return &raw;
  }

  // operator bool

  explicit operator bool() const {
    return raw != nullptr;
  }

  // raw pointer

  T* raw;

 private:
  wheels::SourceLocation loc_;

  void Access(wheels::SourceLocation loc) const {
    MemoryAccess(raw, sizeof(T), loc);
  }
};

// ==

template <typename T>
bool operator==(Ptr<T> lhs, Ptr<T> rhs) {
  return lhs.raw == rhs.raw;
}

template <typename T>
bool operator==(Ptr<T> lhs, T* rhs) {
  return lhs.raw == rhs;
}

template <typename T>
bool operator==(T* lhs, Ptr<T> rhs) {
  return lhs == rhs.raw;
}

// !=

template <typename T>
bool operator!=(Ptr<T> lhs, Ptr<T> rhs) {
  return lhs.raw != rhs.raw;
}

template <typename T>
bool operator!=(Ptr<T> lhs, T* rhs) {
  return lhs.raw != rhs;
}

template <typename T>
bool operator!=(T* lhs, Ptr<T> rhs) {
  return lhs != rhs.raw;
}

// TODO: more operators

}  // namespace user::assist

}  // namespace twist::rt::fiber
