#pragma once

#include <cstdlib>

namespace twist::rt::thread {

namespace mem {

inline void* Malloc(size_t size) {
  return std::malloc(size);
}

inline void Free(void* ptr) {
  std::free(ptr);
}

}  // namespace mem

}  // namespace twist::rt::thread
