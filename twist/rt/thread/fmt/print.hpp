#pragma once

#include <fmt/core.h>

namespace twist::rt::thread {

namespace fmt {

template <typename ... Args>
void Print(::fmt::format_string<Args...> format_str, Args&& ... args) {
  ::fmt::print(format_str, ::std::forward<Args>(args)...);
}

template <typename ... Args>
void Println(::fmt::format_string<Args...> format_str, Args&& ... args) {
  ::fmt::println(format_str, ::std::forward<Args>(args)...);
}

}  // namespace fmt

}  // namespace twist::rt::thread
