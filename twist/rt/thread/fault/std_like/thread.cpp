#include <twist/rt/thread/fault/std_like/thread.hpp>

// TODO: move to fault
#include <twist/test/affinity.hpp>

#include <twist/rt/thread/fault/adversary/adversary.hpp>

namespace twist::rt::thread {
namespace fault {

void FaultyThread::Enter() {
  test::SetThreadAffinity();
  Adversary()->Enter();
}

void FaultyThread::Exit() {
  Adversary()->Exit();
}

}  // namespace fault
}  // namespace twist::thread
