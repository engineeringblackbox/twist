#pragma once

#include <string_view>

namespace twist::rt::thread::assist {

inline void Prune(std::string_view /*why*/) {
  // No-op
}

}  // namespace twist::rt::thread::assist
