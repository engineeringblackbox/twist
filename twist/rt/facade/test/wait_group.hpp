#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/test/wait_group.hpp>

namespace twist::rt::facade {

namespace test {

using fiber::user::test::WaitGroup;

}  // namespace test

}  // namespace twist::rt::facade

#else

#include <twist/rt/thread/test/wait_group.hpp>

namespace twist::rt::facade {

namespace test {

using thread::test::WaitGroup;

}  // namespace test

}  // namespace twist::rt::facade

#endif
