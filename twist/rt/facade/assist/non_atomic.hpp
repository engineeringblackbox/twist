#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/assist/non_atomic.hpp>

namespace twist::rt::facade {

namespace assist {

using rt::fiber::user::assist::NonAtomic;

}  // namespace assist

}  // namespace twist::rt::facade

#else

// TODO

#endif
