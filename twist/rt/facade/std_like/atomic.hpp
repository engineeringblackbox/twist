#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/std_like/atomic.hpp>

namespace twist::rt::facade::std_like {

template <typename T>
using atomic = rt::fiber::user::library::std_like::Atomic<T>;  // NOLINT

}  // namespace twist::rt::facade::std_like

#elif defined(__TWIST_FAULTY__)

#include <twist/rt/thread/fault/std_like/atomic.hpp>

namespace twist::rt::facade::std_like {

template <typename T>
using atomic = rt::thread::fault::FaultyAtomic<T>;  // NOLINT

}  // namespace twist::rt::facade::std_like

#else

#include <atomic>

namespace twist::rt::facade::std_like {

using ::std::atomic;

}  // namespace twist::rt::facade::std_like

#endif
