#pragma once

#include <twist/rt/facade/test/wait_group.hpp>

namespace twist::test {

using rt::facade::test::WaitGroup;

}  // namespace twist::test
