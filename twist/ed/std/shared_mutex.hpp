#pragma once

/*
 * Drop-in replacement for std::shared_mutex
 * https://en.cppreference.com/w/cpp/thread/shared_mutex
 *
 * Contents:
 *   namespace twist::ed::std
 *     class shared_mutex
 */

#include <twist/rt/facade/std_like/shared_mutex.hpp>

namespace twist::ed::std {

using rt::facade::std_like::shared_mutex;

}  // namespace twist::ed::std
