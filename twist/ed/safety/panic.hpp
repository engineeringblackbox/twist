#pragma once

/*
 * [[noreturn]] void Panic(std::string_view error,
 *                         wheels::SourceLocation loc = wheels::SourceLocation::Current());
 *
 * // Writes `error` message to stderr and aborts
 *
 * Usage:
 *
 * int main() {
 *   twist::run::Cross([] {
 *     twist::ed::Panic("for example");  // <- "Panicked at {loc}: for example"
 *   });
 *
 *   return 0;
 * }
 *
 */

#include <twist/rt/facade/safety/panic.hpp>

namespace twist::ed {

using rt::facade::Panic;

}  // namespace twist::ed
