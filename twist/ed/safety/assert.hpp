#pragma once

#include <twist/ed/safety/panic.hpp>

#if !defined(NDEBUG)

#define TWISTED_ASSERT(cond, error) \
  do {                              \
    if (!(cond)) {                  \
      twist::ed::Panic("Assertion \"" #cond "\" failed: " #error); \
    }                               \
  } while (false)

#else

#define TWISTED_ASSERT(cond, error) \
  do {                              \
    /* No-op */                     \
  } while (false)

#endif


#define TWISTED_VERIFY(cond, error) \
  do {                              \
    if (!(cond)) {                  \
      twist::ed::Panic("Assertion \"" #cond "\" failed: " #error); \
    }                               \
  } while (false)
