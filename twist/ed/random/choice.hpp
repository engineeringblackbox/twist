#pragma once

#include <twist/rt/facade/random/choice.hpp>

namespace twist::ed {

namespace random {

using rt::facade::random::Choose;
using rt::facade::random::Either;

}  // namespace random

}  // namespace twist::ed
