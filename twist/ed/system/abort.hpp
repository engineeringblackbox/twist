#pragma once

/*
 * [[noreturn]] void system::Abort();
 *
 * Aborts execution
 *
 * Usage:
 *
 * auto status = twist::run::Sim([] {
 *   twist::ed::system::Abort();
 *
 *   WHEELS_UNREACHABLE();
 * });
 *
 */

#include <twist/rt/facade/system/abort.hpp>

namespace twist::ed {

namespace system {

// [[noreturn]]
using rt::facade::system::Abort;

}  // namespace system

}  // namespace twist::ed
