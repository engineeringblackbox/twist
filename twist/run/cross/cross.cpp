#include <twist/run/cross.hpp>

#include <fmt/core.h>

#if defined(__TWIST_FIBERS__)

#include <twist/run/sim.hpp>

namespace twist::run {

void Cross(cross::Params params, MainRoutine main) {
  // Consistent with MultiThread behavior
  params.sim.sim.crash_on_abort = true;

  Sim(params.sim, main);
}

}  // namespace twist::run

#else

#include <twist/run/mt.hpp>

namespace twist::run {

void Cross(cross::Params /*params*/, MainRoutine main) {
  MultiThread(main);
}

}  // namespace twist::run

#endif


namespace twist::run {

// Standalone run
void Cross(MainRoutine main) {
  Cross(cross::Params{}, std::move(main));
}

}  // namespace twist::run
