#pragma once

#include "../simulator.hpp"
#include "replay.hpp"

#include <twist/rt/fiber/scheduler/random/scheduler.hpp>

#include <optional>

namespace twist::sim::sched {

namespace random {

using twist::rt::fiber::system::scheduler::random::Scheduler;
using SchedulerParams = twist::rt::fiber::system::scheduler::random::Params;

}  // namespace random

}  // namespace twist::sim::sched
