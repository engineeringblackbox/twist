#pragma once

#include "replay.hpp"

#include "../simulator.hpp"

#include <twist/rt/fiber/scheduler/dfs/scheduler.hpp>
#include <twist/rt/fiber/scheduler/replay/schedule.hpp>

#include <optional>

namespace twist::sim::sched {

namespace dfs {

using twist::rt::fiber::system::scheduler::dfs::Scheduler;
using SchedulerParams = twist::rt::fiber::system::scheduler::dfs::Params;

}  // namespace dfs

}  // namespace twist::sim::sched
