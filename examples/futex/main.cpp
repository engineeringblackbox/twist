#include <twist/run/cross.hpp>

#include <twist/ed/std/atomic.hpp>
#include <twist/ed/std/thread.hpp>
#include <twist/ed/wait/futex.hpp>
#include <twist/ed/fmt/print.hpp>

#include <chrono>

using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

class OneShotEvent {
  enum States : uint32_t {
    Init = 0,
    Fired = 1,
  };

 public:
  void Wait() {
    twist::ed::futex::Wait(fired_, /*old=*/States::Init);
  }

  // One-shot
  void Fire() {
    // NB: before store
    auto wake_key = twist::ed::futex::PrepareWake(fired_);

    fired_.store(States::Fired);
    twist::ed::futex::WakeAll(wake_key);
  }

 private:
  twist::ed::std::atomic<uint32_t> fired_{States::Init};
};

//////////////////////////////////////////////////////////////////////

int main() {
  twist::run::Cross([] {
    std::string data;
    OneShotEvent ready;

    twist::ed::std::thread producer([&] {
      twist::ed::std::this_thread::sleep_for(3s);

      data = "Hello";
      ready.Fire();
    });

    ready.Wait();

    twist::ed::fmt::Println("Produced: {}", data);

    producer.join();
  });

  return 0;
}
