#include <twist/run/sim.hpp>

#include <twist/ed/system/write.hpp>

#include <twist/ed/fmt/print.hpp>

#include <cassert>

static_assert(twist::build::Sim());

int main() {
  {
    auto result = twist::run::Sim([] {
      twist::ed::system::Write(1, "Hello, ");
      twist::ed::system::Write(1, "world");

      twist::ed::system::Write(2, "Foo");
      twist::ed::system::Write(2, "Bar");
    });

    static const std::string kExpectedStdOut = "Hello, world";
    assert(result.std_out == kExpectedStdOut);

    static const std::string kExpectedStdErr = "FooBar";
    assert(result.std_err == kExpectedStdErr);
  }

  {
    auto params = twist::run::sim::Params{};
    params.sim.forward_stdout = false;

    auto result = twist::run::Sim(params, [] {
      {
        twist::ed::fmt::Print("Hello, ");
        twist::ed::fmt::Println(/*fd=*/1, "world");
        twist::ed::fmt::Println("{}, {}, {}", 1, 2, 3);

        twist::ed::fmt::Print(2, "Error");
        twist::ed::fmt::Print(2, " ");
        twist::ed::fmt::Println(2, "message");
      }
    });

    static const std::string kExpectedStdOut = "Hello, world\n1, 2, 3\n";
    assert(result.std_out == kExpectedStdOut);

    static const std::string kExpectedStdErr = "Error message\n";
    assert(result.std_err == kExpectedStdErr);
  }

  return 0;
}
