#include <wheels/test/framework.hpp>

#include <twist/run/cross.hpp>

#include <twist/ed/random/choice.hpp>
#include <twist/ed/random/number.hpp>

TEST_SUITE(Random) {
  SIMPLE_TEST(Number) {
    twist::run::Cross([] {
      for (size_t i = 0; i < 100; ++i) {
        while (twist::ed::random::Number() % 100 != i) {
          // Try again
        }
      }
    });
  }

  SIMPLE_TEST(Choice1) {
    twist::run::Cross([] {
      for (size_t i = 0; i < 1000; ++i) {
        size_t k = twist::ed::random::Choose(13);
        ASSERT_TRUE(k < 13);
      }
    });
  }

  SIMPLE_TEST(Choice2) {
    twist::run::Cross([] {
      for (size_t i = 0; i < 10; ++i) {
        while (twist::ed::random::Choose(10) != i) {
          // Try again
        }
      }
    });
  }

  SIMPLE_TEST(Either) {
    twist::run::Cross([] {
      {
        // Wait false
        while (twist::ed::random::Either()) {
          // Try again
        }
      }

      {
        // Wait true
        while (!twist::ed::random::Either()) {
          // Try again
        }
      }
    });
  }
}
