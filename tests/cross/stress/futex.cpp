#include <twist/run/cross.hpp>

#include <twist/ed/std/atomic.hpp>
#include <twist/ed/std/thread.hpp>
#include <twist/ed/wait/futex.hpp>
#include <twist/ed/random/number.hpp>

#include <wheels/core/stop_watch.hpp>

#include <chrono>

using namespace std::chrono_literals;

void TestFutexWait() {
  wheels::StopWatch<> sw;
  while (sw.Elapsed() < 3s) {
    twist::run::Cross([] {
      twist::ed::std::atomic<uint32_t> flag{0};

      twist::ed::std::thread t([&] {
        auto wake_key = twist::ed::futex::PrepareWake(flag);
        flag.store(1);
        twist::ed::futex::WakeOne(wake_key);
      });

      twist::ed::futex::Wait(flag, /*old=*/0);

      t.join();
    });
  }
}

void TestFutexWaitTimed() {
  wheels::StopWatch<> sw;
  while (sw.Elapsed() < 3s) {
    twist::run::Cross([] {
      twist::ed::std::atomic<uint32_t> flag{0};

      twist::ed::std::thread t([&] {
        auto wake_key = twist::ed::futex::PrepareWake(flag);

        auto delay = std::chrono::milliseconds(twist::ed::random::Number(500, 1500));
        twist::ed::std::this_thread::sleep_for(delay);

        flag.store(1);
        twist::ed::futex::WakeOne(wake_key);
      });

      [[maybe_unused]] bool woken = twist::ed::futex::WaitTimed(flag, /*old=*/0, 1s);

      t.join();
    });
  }
}

int main() {
  TestFutexWait();
  TestFutexWaitTimed();
  return 0;
}
